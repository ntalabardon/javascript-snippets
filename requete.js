// url de la requête : http://catalogue.bnf.fr/api/SRU?version=1.2&operation=searchRetrieve&query=bib.isbn%20all%20%229782340033641%22%20&startRecord=1&maximumRecords=5&recordSchema=dublincore
requete = XMLHttpRequest(); // création de l'objet qui va "gérer" notre requête
requete.open(
    'GET', // type de la requête HTTP à lancer
    'http://catalogue.bnf.fr/api/SRU?version=1.2&operation=searchRetrieve&query=bib.isbn%20all%20%229782340033641%22%20&startRecord=1&maximumRecords=5&recordSchema=dublincore', // l'url compliqué dont le code isbn doit être contruit grâce à la fonction de scan de code-barre
    true // si l'on veut que la requête soit lancée de manière asynchrone pour que l'application puisse continuer à "travailler" sans être bloquée par l'attente de la réponse du serveur.
);
requete.responseType = 'document'; //précise que l'on veut que le serveur renvoie sa réponse sous forme d'un arbre xml.
requete.send() // la requête formée progressivement par les instructions ci-dessus est envoyée au serveur.

// visiblement, j'ai un problème d'entête